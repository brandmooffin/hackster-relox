﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="ReloxService.Azure" generation="1" functional="0" release="0" Id="466e4321-44c2-4175-8832-29676fd2dcbf" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="ReloxService.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="ReloxService:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/LB:ReloxService:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="ReloxService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/MapReloxService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="ReloxServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/MapReloxServiceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:ReloxService:Endpoint1">
          <toPorts>
            <inPortMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/ReloxService/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapReloxService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/ReloxService/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapReloxServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/ReloxServiceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="ReloxService" generation="1" functional="0" release="0" software="C:\Projects\Relox\ReloxService\ReloxService.Azure\csx\Release\roles\ReloxService" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;ReloxService&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;ReloxService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/ReloxServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/ReloxServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/ReloxServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="ReloxServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="ReloxServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="ReloxServiceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="3d1c1975-0671-40c4-97ca-7bf48d87787c" ref="Microsoft.RedDog.Contract\ServiceContract\ReloxService.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="e462b80f-1c75-4c12-81c0-a16b31feb4cc" ref="Microsoft.RedDog.Contract\Interface\ReloxService:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/ReloxService.Azure/ReloxService.AzureGroup/ReloxService:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>