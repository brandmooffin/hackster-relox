﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReloxService.Models;
using static System.Web.Security.FormsAuthentication;

namespace ReloxService.Data
{
    public class ReloxDataManager
    {
        public static User LoginUser(User user)
        {
            using (var dbContext = new ReloxDataStoreDataContext())
            {
                var hashedPassword = HashPasswordForStoringInConfigFile(user.Password, "MD5");
                var userDb = dbContext.Relox_Users.ToList()
                    .FirstOrDefault(
                        g =>
                            string.Equals(g.UserEmail.ToLower(), user.Email.ToLower()) &&
                            string.Equals(g.UserPassword, hashedPassword));
                if (userDb != null)
                {
                    user.Id = userDb.UserId;
                    user.CreateDate = userDb.UserCreateDate.ToString();
                }
            }
            return user;
        }


        public static bool AddMessageItem(Item item)
        {
            try
            {
                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    // Create a new db Item object to store into the queue
                    var itemDb = new Relox_Item()
                    {
                        ItemFrom = item.From,
                        UserId = item.UserId,
                        ItemMessage = item.Message,
                        ItemType = item.ItemType,
                        ItemIsRead = "False",
                        ItemCreateDate = DateTime.UtcNow
                    };

                    // Insert our newly created Item and Submit the change to the db
                    dbContext.Relox_Items.InsertOnSubmit(itemDb);
                    dbContext.SubmitChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                // Log error message
            }
            return false;
        }

        public static bool MarkItemMessageRead(int userId, int itemId)
        {
            try
            {
                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    // Look for the item based on the item id and user id
                    var itemDb = dbContext.Relox_Items.FirstOrDefault(i => i.ItemId == itemId && i.UserId == userId);
                    if (itemDb != null)
                    {
                        // we found the item message, so mark it as Read and Submit the change
                        itemDb.ItemIsRead = "True";
                        dbContext.SubmitChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error message
            }

            return false;
        }

        public static bool MarkAllItemMessagesRead(int userId, int itemId)
        {
            try
            {
                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    dbContext.Relox_Items.Where(i => i.UserId == userId).ToList().ForEach(i =>
                    {
                        i.ItemIsRead = "True";
                    });
                    dbContext.SubmitChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                // Log error message
            }
            return false;
        }

        public static Item GetItemMessage(int itemId)
        {
            try
            {
                var item = new Item();
                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    var itemDb = dbContext.Relox_Items.FirstOrDefault(i => i.ItemId == itemId);

                    if (itemDb != null)
                    {
                        item = new Item
                        {
                            ItemType = itemDb.ItemType,
                            Id = itemDb.ItemId,
                            Message = itemDb.ItemMessage,
                            From = itemDb.ItemFrom,
                            CreateDate = itemDb.ItemCreateDate.ToString(),
                            IsRead = bool.Parse(itemDb.ItemIsRead)
                        };
                    }
                }
                return item;
            }
            catch (Exception ex)
            {
                // Log error message
            }
            return null;
        }

        public static List<Item> GetAllUnreadItemMessages(int userId)
        {
            try
            {
                var items = new List<Item>();
                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    dbContext.Relox_Items.Where(i => string.Equals(i.ItemIsRead, "False") && i.UserId == userId)
                        .ToList()
                        .ForEach(i =>
                        {
                            items.Add(new Item()
                            {
                                ItemType = i.ItemType,
                                Id = i.ItemId,
                                Message = i.ItemMessage,
                                From = i.ItemFrom,
                                CreateDate = i.ItemCreateDate.ToString(),
                                IsRead = bool.Parse(i.ItemIsRead)
                            });
                        });
                }
                return items;
            }
            catch (Exception ex)
            {
                // Log error message
            }
            return null;
        } 

        public static Device GetUserDevice(int userId, int deviceId)
        {
            try
            {
                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    // Look for the users device by the device id and user id
                    var deviceDb =
                        dbContext.Relox_Devices.FirstOrDefault(i => i.DeviceId == deviceId && i.UserId == userId);
                    if (deviceDb != null)
                    {
                        // Create a Device object to 
                        var device = new Device
                        {
                            DeviceType = deviceDb.DeviceType,
                            Id = deviceDb.DeviceId,
                            CreateDate = deviceDb.DeviceCreateDate.ToString(),
                            Identifier = deviceDb.DeviceIdentifier,
                            Name = deviceDb.DeviceName,
                            UserId = deviceDb.UserId,
                            IsActive = bool.Parse(deviceDb.DeviceIsActive)
                        };
                        return device;
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error message
            }
            return null;
        }

        public static bool VerifyActiveDeviceBelongsToUser(int userId, int deviceId, string deviceType)
        {
            using (var dbContext = new ReloxDataStoreDataContext())
            {
                var deviceDb =
                    dbContext.Relox_Devices.FirstOrDefault(
                        i => i.DeviceId == deviceId && i.UserId == userId && i.DeviceIsActive.Equals("True") && i.DeviceType.Equals(deviceType));
                if (deviceDb != null)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool VerifyDeviceBelongsToUser(int userId, int deviceId, string deviceType)
        {
            using (var dbContext = new ReloxDataStoreDataContext())
            {
                var deviceDb =
                    dbContext.Relox_Devices.FirstOrDefault(
                        i => i.DeviceId == deviceId && i.UserId == userId && i.DeviceType.Equals(deviceType));
                if (deviceDb != null)
                {
                    return true;
                }
            }
            return false;
        }

        public static Relox_Device FindDeviceByNameAndUserId(string deviceName, int userId, string deviceType = "IOT")
        {
            using (var dbContext = new ReloxDataStoreDataContext())
            {
                var deviceDb =
                    dbContext.Relox_Devices.FirstOrDefault(
                        i =>
                            i.DeviceName.ToUpper() == deviceName.ToUpper() && i.UserId == userId &&
                            i.DeviceType.Equals(deviceType));
                return deviceDb;
            }
        }

        public static void RemoveAllDevicesByType(int userId, string deviceType)
        {
            using (var dbContext = new ReloxDataStoreDataContext())
            {
                var devices = dbContext.Relox_Devices.Where(i => i.UserId == userId && i.DeviceType.Equals(deviceType));
                if (devices.Any())
                {
                    dbContext.Relox_Devices.DeleteAllOnSubmit(devices);
                    dbContext.SubmitChanges();
                }
            }
        }

        public static bool RegisterUser(User user)
        {
            try
            {
                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    var userDb = new Relox_User()
                    {
                        UserEmail = user.Email,
                        UserPassword = HashPasswordForStoringInConfigFile(user.Password, "MD5"),
                        UserCreateDate = DateTime.UtcNow
                    };

                    dbContext.Relox_Users.InsertOnSubmit(userDb);
                    dbContext.SubmitChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                // Log error message
            }
            return false;
        }

    }
}
