﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReloxService.Models
{
    public class Device
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Name { get; set; }

        public string Identifier { get; set; }

        public bool IsActive { get; set; }

        public string DeviceType { get; set; }

        public string CreateDate { get; set; }

        public string MessageResponse { get; set; }
    }
}