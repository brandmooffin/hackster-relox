﻿using System.Runtime.Serialization;

namespace ReloxService.Models
{
    public class Item
    {
        /// <summary>
        /// Item's Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The User Id of the Item's owner; Who does this message belong to
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// The type of Item; Currently SMS or CALL
        /// </summary>
        public string ItemType { get; set; }

        /// <summary>
        /// Person that the message is coming from
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// The body or content of the Item message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Time stamp for the Item
        /// </summary>
        public string CreateDate { get; set; }

        /// <summary>
        /// Flag that determines if the message has been read or not
        /// </summary>
        public bool IsRead { get; set; }
    }
}