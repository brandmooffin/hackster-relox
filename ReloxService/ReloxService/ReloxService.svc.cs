﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using ReloxService.Common;
using ReloxService.Data;
using ReloxService.Models;

namespace ReloxService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ReloxService : IReloxService
    {
        /// <summary>
        /// This will add a new message item (currently SMS or CALL) to the user's queque for devices to pick up
        /// </summary>
        /// <param name="itemStream">Item Json Object Stream</param>
        [WebInvoke(Method = "POST", UriTemplate = "AddItem", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public void AddItem(Stream itemStream)
        {
            try
            {
                // Deserialize json string coming in to an Item
                var streamReader = new StreamReader(itemStream);
                var streamStr = streamReader.ReadToEnd();
                var item = new JavaScriptSerializer().Deserialize<Item>(streamStr);

                if(!ReloxDataManager.AddMessageItem(item)) throw new Exception("Failed to add new Item.");
            }
            catch (Exception ex)
            {
                // Log error message
                Email.ErrorLogEmail("AddItem: " + ex.Message);
            }
        }

        /// <summary>
        /// Mark an Item IsRead to True
        /// </summary>
        /// <param name="itemId">Id of the item message</param>
        /// <param name="userId">User Id of the owner of the item message</param>
        /// <param name="deviceId">Device Id making the request</param>
        /// <param name="deviceType">Device Type making the request</param>
        /// <returns></returns>
        [WebGet(UriTemplate = "MarkItemRead?itemid={itemId}&userId={userId}&deviceId={deviceId}&deviceType={deviceType}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool MarkItemRead(int itemId, int userId, int deviceId, string deviceType)
        {
            try
            {
                // Determine first that the Device is Active and belongs to the user being passed; this is some basic logic to make sure the device has access
                if (!ReloxDataManager.VerifyActiveDeviceBelongsToUser(userId, deviceId, deviceType)) return false;

                // Mark the itema as read
                return ReloxDataManager.MarkItemMessageRead(userId, itemId);
            }
            catch (Exception ex)
            {
                // Log the error
                Email.ErrorLogEmail("MarkItemRead: " + ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Mark all Items IsRead to True
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        [WebGet(UriTemplate = "MarkAllItemsRead?userId={userId}&deviceId={deviceId}&deviceType={deviceType}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool MarkAllItemsRead(int userId, int deviceId, string deviceType)
        {
            try
            {
                // Determine first that the Device is Active and belongs to the user being passed; this is some basic logic to make sure the device has access
                if (!ReloxDataManager.VerifyActiveDeviceBelongsToUser(userId, deviceId, deviceType)) return false;

                // Mark all items read
                return ReloxDataManager.MarkAllItemMessagesRead(userId, deviceId);
            }
            catch (Exception ex)
            {
                // Log the error message
                Email.ErrorLogEmail("MarkAllItemsRead: " + ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Get a specific Item message
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        [WebGet(UriTemplate = "GetItem?itemid={itemId}&userId={userId}&deviceId={deviceId}&deviceType={deviceType}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public Item GetItem(int itemId, int userId, int deviceId, string deviceType)
        {
            // Determine first that the Device is Active and belongs to the user being passed; this is some basic logic to make sure the device has access
            if (!ReloxDataManager.VerifyActiveDeviceBelongsToUser(userId, deviceId, deviceType)) return null;

            // Get the Item message to return
            return ReloxDataManager.GetItemMessage(itemId);
        }

        /// <summary>
        /// Get all unread Item messages
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        /// <param name="deviceType"></param>
        /// <returns></returns>
        [WebGet(UriTemplate = "GetAllUnreadItems?userId={userId}&deviceId={deviceId}&deviceType={deviceType}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public List<Item> GetAllUnreadItems(int userId, int deviceId, string deviceType)
        {
            // Determine first that the Device is Active and belongs to the user being passed; this is some basic logic to make sure the device has access
            if (!ReloxDataManager.VerifyActiveDeviceBelongsToUser(userId, deviceId, deviceType)) return null;

            // Get all unread messages
            return ReloxDataManager.GetAllUnreadItemMessages(userId);
        }

        /// <summary>
        /// Register user
        /// </summary>
        /// <param name="userStream"></param>
        /// <returns></returns>
        [WebInvoke(Method = "POST", UriTemplate = "RegisterUser", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        public bool RegisterUser(Stream userStream)
        {
            try
            {
                StreamReader streamReader = new StreamReader(userStream);
                var streamStr = streamReader.ReadToEnd();
                var user = new JavaScriptSerializer().Deserialize<User>(streamStr);

                // Validate that the password meets the required length; this is my preference and you can totally change it
                if (user.Password.Length < 6) return false;

                // Register user account
                return ReloxDataManager.RegisterUser(user);
            }
            catch (Exception ex)
            {
                // Log error message
                Email.ErrorLogEmail("RegisterUser: " + ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Validate if an email has already been registered
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [WebGet(UriTemplate = "IsEmailRegistered/{email}", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public bool IsEmailRegistered(string email)
        {
            bool result = false;
            try
            {
                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    result = dbContext.Relox_Users.ToList().FirstOrDefault(g => string.Equals(g.UserEmail.ToLower(), email.ToLower())) != null;
                }
            }
            catch (Exception ex)
            {
                // Log error message
                Email.ErrorLogEmail($"IsEmailRegistered {ex.Message} - {ex.StackTrace}");
            }
            return result;
        }

        /// <summary>
        /// Authenticate User
        /// </summary>
        /// <param name="userStream"></param>
        /// <returns></returns>
        [WebInvoke(Method = "POST", UriTemplate = "Login", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public User Login(Stream userStream)
        {
            User user = null;
            try
            {
                StreamReader streamReader = new StreamReader(userStream);
                var streamStr = streamReader.ReadToEnd();
                user = new JavaScriptSerializer().Deserialize<User>(streamStr);

                user = ReloxDataManager.LoginUser(user);
            }
            catch (Exception ex) { Email.ErrorLogEmail("RegisterUser: " + ex.Message); }

            return user;
        }

        [WebInvoke(Method = "POST", UriTemplate = "AddDevice", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Device AddDevice(Stream deviceStream)
        {
            try
            {
                 StreamReader streamReader = new StreamReader(deviceStream);
                var streamStr = streamReader.ReadToEnd();
                var device = new JavaScriptSerializer().Deserialize<Device>(streamStr);

                var foundDevice = ReloxDataManager.FindDeviceByNameAndUserId(device.Name, device.UserId, device.DeviceType);

                if (device.DeviceType.Equals("PHONE"))
                {
                    if (foundDevice != null)
                    {
                        if (foundDevice.DeviceIsActive.Equals("True"))
                        {
                            device.Id = foundDevice.DeviceId;
                            return device;
                        }
                    }

                    ReloxDataManager.RemoveAllDevicesByType(device.UserId, device.DeviceType);
                }else if (device.DeviceType.Equals("IOT"))
                {
                    if (foundDevice != null)
                    {
                        device.MessageResponse =
                            "You already have another device with the same name. You must use the mobile to remove the device before you can use this name again.";
                        return device;
                    }
                }

                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    var deviceDb = new Relox_Device
                    {
                        DeviceIdentifier = device.Identifier,
                        UserId = device.UserId,
                        DeviceName = device.Name,
                        DeviceIsActive = "True",
                        DeviceType = device.DeviceType,
                        DeviceCreateDate = DateTime.UtcNow
                    };

                    dbContext.Relox_Devices.InsertOnSubmit(deviceDb);
                    dbContext.SubmitChanges();
                    device.Id = deviceDb.DeviceId;
                }
                return device;

            }
            catch (Exception ex) { Email.ErrorLogEmail("AddDevice: " + ex.Message); }
            return null;
        }

        [WebInvoke(Method = "POST", UriTemplate = "UpdateDevice", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool UpdateDevice(Stream deviceStream)
        {
            try
            {
                StreamReader streamReader = new StreamReader(deviceStream);
                var streamStr = streamReader.ReadToEnd();
                var device = new JavaScriptSerializer().Deserialize<Device>(streamStr);

                if (!ReloxDataManager.VerifyDeviceBelongsToUser(device.UserId, device.Id, device.DeviceType)) return false;

                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    var deviceDb = dbContext.Relox_Devices.FirstOrDefault(i => i.DeviceId == device.Id && i.UserId == device.UserId);
                    if (deviceDb != null)
                    {
                        deviceDb.DeviceName = device.Name;
                        deviceDb.DeviceIsActive = device.IsActive.ToString();
                        dbContext.SubmitChanges();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Email.ErrorLogEmail("UpdateDevice: " + ex.Message);
            }

            return false;
        }

        [WebGet(UriTemplate = "RemoveDevice?userId={userId}&deviceId={deviceId}&deviceType={deviceType}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public bool RemoveDevice(int userId, int deviceId, string deviceType)
        {
            try
            {
                if (!ReloxDataManager.VerifyDeviceBelongsToUser(userId, deviceId, deviceType)) return false;

                using (var dbContext = new ReloxDataStoreDataContext())
                {
                    var deviceDb = dbContext.Relox_Devices.FirstOrDefault(i => i.DeviceId == deviceId && i.UserId == userId);
                    if (deviceDb != null)
                    {
                        dbContext.Relox_Devices.DeleteOnSubmit(deviceDb);
                        dbContext.SubmitChanges();
                        return true;
                    }
                }

            }
            catch (Exception ex)
            {
                Email.ErrorLogEmail("RemoveDevice: " + ex.Message);
            }

            return false;
        }

        [WebGet(UriTemplate = "GetAllDevices?userId={userId}&deviceType={deviceType}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public List<Device> GetAllDevices(int userId, string deviceType)
        {
            var devices = new List<Device>();
            using (var dbContext = new ReloxDataStoreDataContext())
            {
                dbContext.Relox_Devices.Where(i => string.Equals(i.DeviceType, deviceType) && i.UserId == userId).ToList().ForEach(i =>
                {
                    devices.Add(new Device()
                    {
                        DeviceType = i.DeviceType,
                        Id = i.DeviceId,
                        CreateDate = i.DeviceCreateDate.ToString(),
                        Identifier = i.DeviceIdentifier,
                        Name = i.DeviceName,
                        UserId = i.UserId,
                        IsActive = bool.Parse(i.DeviceIsActive)
                    });
                });
            }
            return devices;
        }

        [WebGet(UriTemplate = "GetDevice?userId={userId}&deviceId={deviceId}&deviceType={deviceType}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        public Device GetDevice(int userId, int deviceId, string deviceType)
        {
            try
            {
                if (!ReloxDataManager.VerifyDeviceBelongsToUser(userId, deviceId, deviceType)) return null;

                return ReloxDataManager.GetUserDevice(userId, deviceId);
            }
            catch (Exception ex)
            {
                Email.ErrorLogEmail("GetDevice: " + ex.Message);
            }

            return null;
        }
    }
}
