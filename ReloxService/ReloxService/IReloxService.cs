﻿using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using ReloxService.Models;

namespace ReloxService
{
    [ServiceContract]
    public interface IReloxService
    {

        [OperationContract]
        void AddItem(Stream itemStream);

        [OperationContract]
        bool MarkItemRead(int itemId, int userId, int deviceId, string deviceType);

        [OperationContract]
        bool MarkAllItemsRead(int userId, int deviceId, string deviceType);

        [OperationContract]
        Item GetItem(int itemId, int userId, int deviceId, string deviceType);

        [OperationContract]
        List<Item> GetAllUnreadItems(int userId, int deviceId, string deviceType);

        [OperationContract]
        bool RegisterUser(Stream userStream);

        [OperationContract]
        bool IsEmailRegistered(string email);

        [OperationContract]
        User Login(Stream userStream);

        [OperationContract]
        Device AddDevice(Stream deviceStream);

        [OperationContract]
        bool UpdateDevice(Stream deviceStream);

        [OperationContract]
        bool RemoveDevice(int userId, int deviceId, string deviceType);

        [OperationContract]
        List<Device> GetAllDevices(int userId, string deviceType);

        [OperationContract]
        Device GetDevice(int userId, int deviceId, string deviceType);
    }
}
