package apps.brokenwallsstudios.relox.model;

/**
 * Created by brandon on 9/12/2015.
 */
public class Device {
    public int Id;
    public int UserId;
    public String Name;
    public String Identifier;
    public boolean IsActive;
    public String CreateDate;
    public String DeviceType = "PHONE";
}
