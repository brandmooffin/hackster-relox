package apps.brokenwallsstudios.relox;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import apps.brokenwallsstudios.relox.model.User;

public class DeviceDetailActivity extends Activity {

    AppSettings appSettings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_detail);

        appSettings = new AppSettings(this);

        final int deviceId = getIntent().getExtras().getInt("deviceId");
        String deviceName = getIntent().getExtras().getString("deviceName");
        String deviceCreateDate = getIntent().getExtras().getString("deviceCreateDate");

        TextView mDeviceNameText = (TextView) findViewById(R.id.deviceNameText);
        TextView mDeviceDateText = (TextView) findViewById(R.id.deviceDateText);

        Button mUnregisterDeviceButton = (Button) findViewById(R.id.unregisterDeviceButton);
        mUnregisterDeviceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Unregister device
                UnregisterDeviceTask mUnregisterDeviceTask = new UnregisterDeviceTask(deviceId);
                mUnregisterDeviceTask.execute((Void) null);
            }
        });

        mDeviceNameText.setText(deviceName);
        mDeviceDateText.setText("Added: " + deviceCreateDate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_device_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class UnregisterDeviceTask extends AsyncTask<Void, Void, Boolean> {

        private final int DeviceId;

        UnregisterDeviceTask(int deviceId) {
            DeviceId = deviceId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                URL url = new URL("http://reloxservice.cloudapp.net/ReloxService/RemoveDevice?userId="+appSettings.UserId+"&deviceId="+DeviceId+"&deviceType=IOT");
                URLConnection connection = url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);

                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = in.readLine()) != null) {
                    sb.append(line);
                }
                in.close();
                return Boolean.parseBoolean(sb.toString());
            } catch (Exception e) {
                System.out.println("\nError while calling service");
                System.out.println(e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if (success) {
               finish();
            }else{
                Toast.makeText(getBaseContext(), "Unable to unregister the device. Please try again later.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
