package apps.brokenwallsstudios.relox.receiver;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import apps.brokenwallsstudios.relox.AppSettings;
import apps.brokenwallsstudios.relox.model.Item;

/**
 * Created by brandon on 8/29/2015.
 */
public class SmsMessageReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        AppSettings appSettings = new AppSettings(context);
        if(appSettings.IsScreenOn)return;

        String action = intent == null ? null : intent.getAction();

        // Listen for SMS received and then send out a notification
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(action)) {
            Bundle bundle = intent.getExtras();

            SmsMessage[] msgs = null;

            String str = "";

            if (bundle != null) {
                // Retrieve the SMS Messages received
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];

                String contact="";
                String message ="";
                // For every SMS message received
                for (int i=0; i < msgs.length; i++) {
                    // Convert Object array
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    contact = getContactIdentifier(context.getContentResolver(), msgs[i].getOriginatingAddress());
                    // Sender's phone number
                    str += "SMS from " + contact + " : ";
                    // Fetch the text message
                    str += msgs[i].getMessageBody().toString();
                    message += msgs[i].getMessageBody().toString();
                    // Newline <img src="http://codetheory.in/wp-includes/images/smilies/simple-smile.png" alt=":-)" class="wp-smiley" style="height: 1em; max-height: 1em;">
                    str += "\n";
                }

                Item item =new Item();
                item.UserId = appSettings.UserId;
                item.From = contact;
                item.ItemType = "SMS";
                item.Message=message;

                Gson gson = new Gson();
                final String json = gson.toJson(item);

                // Send notification as POST to REST service
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run() {
                        try {
                            try {
                                URL url = new URL("http://reloxservice.cloudapp.net/ReloxService/AddItem");
                                URLConnection connection = url.openConnection();
                                connection.setDoOutput(true);
                                connection.setRequestProperty("Content-Type", "application/json");
                                connection.setConnectTimeout(5000);
                                connection.setReadTimeout(5000);
                                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                                out.write(json);
                                out.close();

                                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                StringBuilder sb = new StringBuilder();
                                String line;
                                while ((line = in.readLine()) != null) {
                                    sb.append(line);
                                }
                                System.out.println("\nService Invoked Successfully..");
                                System.out.println("\n"+sb.toString());
                                in.close();
                            } catch (Exception e) {
                                System.out.println("\nError while calling service");
                                System.out.println(e);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                thread.start();

                // Step2: Now pass JSON File Data to REST Service


            }
        } else if (Telephony.Sms.Intents.WAP_PUSH_RECEIVED_ACTION.equals(action)) {
            //handleIncomingMms(context, intent);
        }
    }

    // The sender's name if we have them in our contacts
    public String getContactIdentifier(ContentResolver cr, String phoneNumber) {
        String phoneNumberOrName = phoneNumber;

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);

        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        if(contactName != null){
            phoneNumberOrName = contactName;
        }

        return phoneNumberOrName;
    }
}
