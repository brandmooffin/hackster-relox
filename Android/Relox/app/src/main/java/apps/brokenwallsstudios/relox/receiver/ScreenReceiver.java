package apps.brokenwallsstudios.relox.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import apps.brokenwallsstudios.relox.AppSettings;

/**
 * Created by brandon on 9/3/2015.
 */
public class ScreenReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        final AppSettings appSettings = new AppSettings(context);
        boolean isScreenOn = false;
        // NOTE: Something interesting to keep in mind, when the phone is ringing this event also gets called as the phone call wakes up the screen
        // Listen for the screen on event so we know to clear out all messages
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            isScreenOn = false;
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {

            // The phone is ringing, so we don't want to clear messages because of this
            if(appSettings.IsPhoneRinging)return;

            isScreenOn = true;

            //mark all messages read
            Thread thread = new Thread(new Runnable(){
                @Override
                public void run() {
                    try {
                        try {
                            URL url = new URL("http://reloxservice.cloudapp.net/ReloxService/MarkAllItemsRead?userId="+appSettings.UserId
                                    +"&deviceId="+appSettings.DeviceId+"&deviceType=PHONE");
                            URLConnection connection = url.openConnection();
                            connection.setRequestProperty("Content-Type", "application/json");
                            connection.setConnectTimeout(5000);
                            connection.setReadTimeout(5000);

                            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                            StringBuilder sb = new StringBuilder();
                            String line;
                            while ((line = in.readLine()) != null) {
                                sb.append(line);
                            }
                            System.out.println("\nService Invoked Successfully..");
                            System.out.println("\n"+sb.toString());
                            in.close();
                        } catch (Exception e) {
                            System.out.println("\nError while calling service");
                            System.out.println(e);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            thread.start();
        }

        //save screen state for other receivers
        appSettings.IsScreenOn = isScreenOn;
        appSettings.SaveSettings();
    }

}
