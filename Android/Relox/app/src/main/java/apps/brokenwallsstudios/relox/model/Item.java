package apps.brokenwallsstudios.relox.model;

/**
 * Created by brandon on 9/1/2015.
 */
public class Item {
    public int Id;
    public int UserId;
    public String ItemType;
    public String From;
    public String Message;
    public String CreateDate;
    public boolean IsRead;
}
