package apps.brokenwallsstudios.relox.receiver;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import apps.brokenwallsstudios.relox.AppSettings;
import apps.brokenwallsstudios.relox.model.Item;

/**
 * Created by brandon on 9/1/2015.
 */
public class IncomingCallReceiver extends BroadcastReceiver {

    public void onReceive(final Context context, Intent intent) {

        final AppSettings appSettings = new AppSettings(context);
        if(appSettings.IsScreenOn)return;


        try {
            // TELEPHONY MANAGER class object to register one listner
            final TelephonyManager tmgr = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);

            // Register listener for LISTEN_CALL_STATE
            tmgr.listen(new PhoneStateListener(){
                @Override
                public void onCallStateChanged(int state, String incomingNumber) {
                    super.onCallStateChanged(state, incomingNumber);

                    // Check if the Phone is currently ringing and that it was already ringing;
                    // NOTE: This check occurs as many times as possible while the phone is ringing, so we need to add a flag so that the notification gets fired once
                    if(state == TelephonyManager.CALL_STATE_RINGING && !appSettings.IsPhoneRinging) {

                        appSettings.IsPhoneRinging = true;
                        appSettings.SaveSettings();

                        String contact = getContactIdentifier(context.getContentResolver(),incomingNumber);
                        String incomingCallMessage = "Call From: " + contact;
                        Toast.makeText(context, incomingCallMessage, Toast.LENGTH_LONG).show();


                        Item item =new Item();
                        item.UserId = appSettings.UserId;
                        item.From = contact;
                        item.ItemType = "CALL";
                        item.Message="Call From " + contact;

                        Gson gson = new Gson();
                        final String json = gson.toJson(item);

                        // Send out the notification through the service for the device
                        Thread thread = new Thread(new Runnable(){
                            @Override
                            public void run() {
                                try {
                                    try {
                                        URL url = new URL("http://reloxservice.cloudapp.net/ReloxService/AddItem");
                                        URLConnection connection = url.openConnection();
                                        connection.setDoOutput(true);
                                        connection.setRequestProperty("Content-Type", "application/json");
                                        connection.setConnectTimeout(5000);
                                        connection.setReadTimeout(5000);
                                        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                                        out.write(json);
                                        out.close();

                                        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                        StringBuilder sb = new StringBuilder();
                                        String line;
                                        while ((line = in.readLine()) != null) {
                                            sb.append(line);
                                        }
                                        System.out.println("\nService Invoked Successfully..");
                                        System.out.println("\n"+sb.toString());
                                        in.close();
                                    } catch (Exception e) {
                                        System.out.println("\nError while calling service");
                                        System.out.println(e);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        thread.start();
                    }else if(state == TelephonyManager.CALL_STATE_OFFHOOK || state == TelephonyManager.CALL_STATE_IDLE){
                        // If the phone is no longer ringing or idle then we want to reset our flag so the next time the phone rings we send out a notification
                        appSettings.IsPhoneRinging = false;
                        appSettings.SaveSettings();
                    }
                }
            }, PhoneStateListener.LISTEN_CALL_STATE);

        } catch (Exception e) {
            Log.e("Phone Receive Error", " " + e);
        }

    }

    // The sender's name if we have them in our contacts
    public String getContactIdentifier(ContentResolver cr, String phoneNumber) {
        String phoneNumberOrName = phoneNumber;

        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME }, null, null, null);

        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor
                    .getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        if(contactName != null){
            phoneNumberOrName = contactName;
        }

        return phoneNumberOrName;
    }
}