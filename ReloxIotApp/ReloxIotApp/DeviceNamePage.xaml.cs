﻿using System;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReloxIotApp.Models;


namespace ReloxIotApp
{
    public sealed partial class DeviceNamePage : Page
    {
        readonly AppSettings _appSettings = new AppSettings();
        public DeviceNamePage()
        {
            InitializeComponent();
        }

        private async void AddDeviceButton_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            // This project has some minor validation for the device, verifying that the name isn't already being used and that it's at least 6 characters in length
            // This requirement be adjusted to whatever your preference
            if (TextBoxDeviceName.Text.Length >= 6)
            {
                try
                {
                    // Create a Device object to register it
                    var device = new Device()
                    {
                       UserId = _appSettings.UserId,
                       Identifier = TextBoxDeviceName.Text,
                       Name = TextBoxDeviceName.Text,
                       DeviceType = "IOT"
                    };

                    // POST request with Device to register it and add it to the database
                    // Registration is important to keep track of the devices that the user has; 
                    // so we know which devices have access to what content and for user management through the Android app
                    string url = App.ServiceBaseUrl + "AddDevice";
                    var requestAddDevice = WebRequest.Create(url);

                    byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(device));

                    requestAddDevice.Method = "POST";
                    requestAddDevice.ContentType = "application/json; charset=utf-8";

                    using (Stream stream = await Task.Factory.FromAsync<Stream>(requestAddDevice.BeginGetRequestStream, requestAddDevice.EndGetRequestStream, null))
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    requestAddDevice.BeginGetResponse(ResponseAddDeviceCallback, requestAddDevice);
                }
                catch (WebException webException)
                {
                    ErrorMessage.Text = "There was a communication problem. " + webException.Message + webException.StackTrace;
                }
                catch (CommunicationException commProblem)
                {
                    ErrorMessage.Text = "There was a communication problem. " + commProblem.Message + commProblem.StackTrace;
                }
            }
            else
            {
                ErrorMessage.Text = "Device name must be at least 6 characters";
            }
        }

        private async void ResponseAddDeviceCallback(IAsyncResult result)
        {
            try
            {
                // Grab the response
                var request = (HttpWebRequest)result.AsyncState;
                var response = (HttpWebResponse)request.EndGetResponse(result);

                var jsonResponse = new StreamReader(response.GetResponseStream()).ReadToEnd();
                Device device = null;
                // The web service used in this project returns a null object if the sent criteria is invalid in any way
                // So we check if the response is empty before we attempt to parse
                if (!string.IsNullOrEmpty(jsonResponse))
                {
                    device = JObject.Parse(jsonResponse).ToObject<Device>();
                }

                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    // Verify that we did get a device back with an id
                    if (device != null && device.Id > 0)
                    {
                        // Lets assign the Id to the device and update its settings
                        _appSettings.DeviceId = device.Id;
                        _appSettings.DeviceIdentifier = device.Identifier;
                        _appSettings.DeviceName = device.Name;
                        _appSettings.IsActive = true;
                        _appSettings.SaveSettings();

                        // Redirect user to the main page now that they have a device set up
                        App.RootFrame.Navigate(typeof(MainPage));
                    }
                    else
                    {
                        if (device != null && !string.IsNullOrEmpty(device.MessageResponse))
                        {
                            ErrorMessage.Text = device.MessageResponse;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => ErrorMessage.Text = "Unable to add device. This may be due to an inactive network connection. Please verify and try again.");
            }
        }
    }
}
