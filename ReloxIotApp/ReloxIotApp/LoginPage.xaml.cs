﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReloxIotApp.Models;


namespace ReloxIotApp
{
    public sealed partial class LoginPage : Page
    {
        readonly AppSettings _appSettings = new AppSettings();

        public LoginPage()
        {
            InitializeComponent();
        }

        private async void RegisterButton_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            // This project has some minor validation for the user, verifying that the email is valid and that the password is at least 6 characters in length
            // This requirement be adjusted to whatever your preference, be aware that validation should also be reflected in the REST service
            if (IsEmailValid(TextBoxEmail.Text))
            {
                if (TextBoxPassword.Password.Length >= 6)
                {
                    try
                    {
                        // Create a User object to validate credentials with
                        var user = new User()
                        {
                            Email = TextBoxEmail.Text,
                            Password = TextBoxPassword.Password
                        };

                        // POST request with User object to validate user credentials
                        var url = App.ServiceBaseUrl + "Login";
                        var requestLogin = WebRequest.Create(url);

                        byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(user));

                        requestLogin.Method = "POST";
                        requestLogin.ContentType = "application/json; charset=utf-8";
                        
                        using (Stream stream = await Task.Factory.FromAsync<Stream>(requestLogin.BeginGetRequestStream, requestLogin.EndGetRequestStream, null))
                        {
                            stream.Write(data, 0, data.Length);
                        }

                        requestLogin.BeginGetResponse(ResponseLoginCallback, requestLogin);
                    }
                    catch (WebException webException)
                    {
                        ErrorMessage.Text = "There was a communication problem. " + webException.Message + webException.StackTrace;
                    }
                    catch (CommunicationException commProblem)
                    {
                        ErrorMessage.Text = "There was a communication problem. " + commProblem.Message + commProblem.StackTrace;
                    }
                }
                else
                {
                    ErrorMessage.Text = "Invalid email or password";
                }
            }
            else
            {
                ErrorMessage.Text = "Invalid email or password";
            }
        }

        private async void ResponseLoginCallback(IAsyncResult result)
        {
            try
            {
                var request = (HttpWebRequest)result.AsyncState;
                var response = (HttpWebResponse)request.EndGetResponse(result);

                // Checking that we actually received a valid json response before doing anything with it
                var jsonResponse = new StreamReader(response.GetResponseStream()).ReadToEnd();
                User user = null;
                if (!string.IsNullOrWhiteSpace(jsonResponse))
                {
                    user = JObject.Parse(jsonResponse).ToObject<User>();
                }

                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    // Verify we have a valid User and Id
                    if (user != null && user.Id > 0)
                    {
                        // Store the User info for the device later so it can grab messages
                        _appSettings.UserId = user.Id;
                        _appSettings.UserEmail = user.Email;
                        _appSettings.SaveSettings();

                        App.RootFrame.Navigate(typeof (DeviceNamePage));
                    }
                    else
                    {
                        ErrorMessage.Text = "Please check your email and password then try again.";
                    }
                });
            }
            catch (TargetInvocationException)
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => ErrorMessage.Text = "This may be due to an inactive network connection. Please verify and try again.");
            }
        }

        private bool IsEmailValid(string email)
        {
            // Regex pattern for validating a proper email
            var MatchEmailPattern = @"\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b";

            // If the user doesnt provide an email then we already know it's invalid
            if (string.IsNullOrEmpty(email))
            {
                return false;
            }

            // otherwise we compare it to the Regex pattern
            return Regex.IsMatch(email, MatchEmailPattern);
        }
    }
}
