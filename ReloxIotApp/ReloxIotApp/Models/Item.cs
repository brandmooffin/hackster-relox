﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReloxIotApp.Models
{
    public class Item
    {
        public int Id { get; set; }

        public string ItemType { get; set; }

        public string From { get; set; }
  
        public string Message { get; set; }

        public string CreateDate { get; set; }

        public bool IsRead { get; set; }
    }
}
