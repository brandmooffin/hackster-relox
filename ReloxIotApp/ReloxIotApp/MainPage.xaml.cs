﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using Windows.Devices.Gpio;
using Windows.Media.SpeechSynthesis;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Newtonsoft.Json.Linq;
using ReloxIotApp.Models;

namespace ReloxIotApp
{
    public sealed partial class MainPage : Page
    {
        private readonly AppSettings _appSettings = new AppSettings();
        private const int RedledPin = 5;
        private const int BlueledPin = 6;

        private GpioPin _redpin;
        private GpioPin _bluepin;
 
        private readonly DispatcherTimer _timer;
        private readonly SolidColorBrush _redBrush = new SolidColorBrush(Colors.Red);
        private readonly SolidColorBrush _blueBrush = new SolidColorBrush(Colors.Blue);
        private readonly SolidColorBrush _grayBrush = new SolidColorBrush(Colors.LightGray);

        private List<Item> _messageItems = new List<Item>();
        private bool _isGettingMessages;

        private readonly SpeechSynthesizer _synthesizer;

        public MainPage()
        {
            InitializeComponent();

            DeviceNameText.Text = _appSettings.DeviceName;

            _synthesizer = new SpeechSynthesizer();

            // Setup our speech voice for reading out messages
            var voices = SpeechSynthesizer.AllVoices;
            VoiceInformation voice = voices.First();
            _synthesizer.Voice = voice;

            // Setup the timer task that will poll the web service for new messages and status
            _timer = new DispatcherTimer {Interval = TimeSpan.FromMilliseconds(5000)};
            _timer.Tick += Timer_Tick;
            InitGpio();
            if (_redpin != null)
            {
                _timer.Start();
            }
        }

        private void InitGpio()
        {
            // Get our Gpio Controller for opening the pins that will be used
            var gpio = GpioController.GetDefault();
            
            // Show an error if there is no GPIO controller
            if (gpio == null)
            {
                _redpin = null;
                _bluepin = null;
                return;
            }
            
            // Setup our two color pins (red and blue)
            _redpin = gpio.OpenPin(RedledPin);
            _redpin.Write(GpioPinValue.Low);
            _redpin.SetDriveMode(GpioPinDriveMode.Output);

            _bluepin = gpio.OpenPin(BlueledPin);
            _bluepin.Write(GpioPinValue.Low);
            _bluepin.SetDriveMode(GpioPinDriveMode.Output);
        }

        private void Timer_Tick(object sender, object e)
        {
            // We don't want to destroy the service we're calling, so if we are in the middle of a call, we should wait until that's done first
            if (!_isGettingMessages)
            {
                try
                {
                    // Most important, check the status of the IoT device first. 
                    // INFO: This project has an Android app that pairs with the IoT app that lets you manage devices that you have hooked up. 
                    // So we make this check first because the user could remotely unregister the device which should mean that the device should no longer have access to your messages
                    string url = string.Format(App.ServiceBaseUrl + "GetDevice?userId={0}&deviceId={1}&deviceType={2}", _appSettings.UserId, _appSettings.DeviceId, "IOT");
                    var request = WebRequest.Create(url);
                    request.BeginGetResponse(ResponseGetDeviceCallback, request);
                    // We flag we are currently getting messages now, checking device status is included
                    _isGettingMessages = true;

                }
                catch (Exception ex)
                {
                    // If anything happens, we will spit out the error on screen and flag that we are no longer grabbing messages
                    ResetMessageStatus();
                    MessageStatus.Text = "No messages; " + ex.Message;
                    _isGettingMessages = false;
                }
            }
        }

        private async void ResponseGetDeviceCallback(IAsyncResult result)
        {
            try
            {
                // Get the response back 
                var request = (HttpWebRequest) result.AsyncState;
                var response = (HttpWebResponse) request.EndGetResponse(result);
                var jsonResponse = new StreamReader(response.GetResponseStream()).ReadToEnd();

                Device device = null;
                // The web service used in this project returns a null object if the sent criteria is invalid in any way
                // So we check if the response is empty before we attempt to parse
                if (!string.IsNullOrEmpty(jsonResponse))
                {
                    device = JObject.Parse(jsonResponse).ToObject<Device>();
                }

                // If we have a device, then lets check that it's also active so we know it most likely has access to messages
                if (device != null && device.IsActive)
                {
                    string url =
                        string.Format(App.ServiceBaseUrl + "GetAllUnreadItems?userId={0}&deviceId={1}&deviceType={2}",
                            _appSettings.UserId, _appSettings.DeviceId, "IOT");
                    var requestWeb = WebRequest.Create(url);
                    requestWeb.BeginGetResponse(ResponseGetAllUnreadItemsCallback, requestWeb);
                }
                else
                {
                    // If our device is not active, then the user must have deactivated and we need to clear out this device for reuse
                    _appSettings.RestoreSettingsToDefaults();
                    _appSettings.SaveSettings();
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        // Some nice clean up 
                        ResetMessageStatus();

                        // The device no longer has a need for the open pins, so we need to dispose them to free them up
                        _redpin.Dispose();
                        _bluepin.Dispose();

                        // Kill the timer task so we stop polling the web service
                        _timer.Stop();

                        // Redirect to the initial setup page
                        App.RootFrame.Navigate(typeof (LoginPage));
                    });
                }

            }
            catch (Exception ex)
            {
                // If anything happens, we will spit out the error on screen and flag that we are no longer grabbing messages
                ResetMessageStatus();
                MessageStatus.Text = "No messages; " + ex.Message;
                _isGettingMessages = false;
            }
        }

        private async void ResponseGetAllUnreadItemsCallback(IAsyncResult result)
        {
            try
            {
                var request = (HttpWebRequest)result.AsyncState;
                var response = (HttpWebResponse)request.EndGetResponse(result);
                var jsonResponse = new StreamReader(response.GetResponseStream()).ReadToEnd();
                var messageItems = JObject.Parse(jsonResponse)["GetAllUnreadItemsResult"];
                _messageItems = JArray.FromObject(messageItems).ToObject<List<Item>>();

                //if unread messages found; update message status and turn on light
                //else no messages found and turn off light
                bool messagesFound = _messageItems.Any();
                if (messagesFound)
                {
                    _messageItems = _messageItems.ToList().OrderByDescending(i => i.CreateDate).ToList();

                    var item = new Item
                    {
                        Id = _messageItems.First().Id,
                        Message = _messageItems.First().Message,
                        From = _messageItems.First().From,
                        ItemType = _messageItems.First().ItemType
                    };

                    if (item.Id == _appSettings.LastReadItemId)
                    {
                        _isGettingMessages = false;
                        return;
                    }
                    
                    string messageStatus = string.Format("From {0}: {1}", item.From, item.Message);
                    string messageCount = string.Format("{0} messages", _messageItems.Count);

                    _appSettings.LastReadItemId = _messageItems.First().Id;
                    _appSettings.SaveSettings();

                    if (string.Equals(item.ItemType, "SMS"))
                    {
                        _redpin.Write(GpioPinValue.High);
                        _bluepin.Write(GpioPinValue.Low);
                    }
                    else if (string.Equals(item.ItemType, "CALL"))
                    {
                        _bluepin.Write(GpioPinValue.High);
                        _redpin.Write(GpioPinValue.Low);

                        messageStatus =  item.Message;
                    }
                    else
                    {
                        _redpin.Write(GpioPinValue.Low);
                        _bluepin.Write(GpioPinValue.Low);
                    }


                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        if (!string.Equals(MessageCount.Text, messageCount))
                        {
                            MessageCount.Text = messageCount;
                        }
                        if (!string.Equals(messageStatus, MessageStatus.Text))
                        {
                            media.CurrentStateChanged += media_CurrentStateChanged;

                            SpeechSynthesisStream synthesisStream;
                            try
                            {
                                //creating a stream from the text which can be played using media element. This new API converts text input into a stream.
                                synthesisStream = await _synthesizer.SynthesizeTextToStreamAsync(messageStatus);
                            }
                            catch (Exception)
                            {
                                synthesisStream = null;
                            }

                            // if the SSML stream is not in the correct format throw an error message to the user
                            if (synthesisStream == null)
                            {
                                Debug.WriteLine("unable to synthesize text");
                                return;
                            }

                            media.AutoPlay = true;
                            media.SetSource(synthesisStream, synthesisStream.ContentType);
                            media.Play();

                            MessageStatus.Text = messageStatus;
                        }

                        if (string.Equals(item.ItemType, "SMS"))
                        {
                            if (LED.Fill != _redBrush)
                            {
                                LED.Fill = _redBrush;
                            }
                        }else if (string.Equals(item.ItemType, "CALL"))
                        {
                            if (LED.Fill != _blueBrush)
                            {
                                LED.Fill = _blueBrush;
                            }
                        }
                        else
                        {
                            LED.Fill = _grayBrush;
                        }
                    });
                }
                else
                {
                    _redpin.Write(GpioPinValue.Low);
                    _bluepin.Write(GpioPinValue.Low);

                    _messageItems.Clear();
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        MessageCount.Text = "0 messages";
                        MessageStatus.Text = "No messages";
                        LED.Fill = _grayBrush;
                    });
                }
            }
            catch (Exception ex)
            {
                // If anything happens, we will spit out the error on screen and flag that we are no longer grabbing messages
                ResetMessageStatus();
                MessageStatus.Text = "No messages; " + ex.Message;
                _isGettingMessages = false;
            }
            _isGettingMessages = false;
        }

        void media_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            if (media.CurrentState == MediaElementState.Paused)
            {
                media.CurrentStateChanged -= media_CurrentStateChanged;
            }
        }

        private void UnreadMessages_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            if (UnreadMessagesPanel.Visibility == Visibility.Collapsed)
            {
                UnreadMessagesListView.ItemsSource = _messageItems;
                UnreadMessagesPanel.Visibility = Visibility.Visible;
                UnreadMessageButton.Content = "Hide Unread Messages";
            }
            else
            {
                UnreadMessagesPanel.Visibility = Visibility.Collapsed;
                UnreadMessageButton.Content = "View Unread Messages";
            }
        }

        private void MarkAllMessagesRead_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_messageItems.Any())
            {
                try
                {
                    // If there are any unread messages, then lets tell the service to mark them all read
                    string url = string.Format(App.ServiceBaseUrl + "MarkAllItemsRead?userId={0}&deviceId={1}&deviceType={2}", _appSettings.UserId, _appSettings.DeviceId, "IOT");
                    var request = WebRequest.Create(url);
                    request.BeginGetResponse(ResponseMarkAllItemsReadCallback, request);
                }
                catch (Exception ex)
                {
                    MessageStatus.Text += "; " + ex.Message;
                }
            }
        }

        private async void ResponseMarkAllItemsReadCallback(IAsyncResult ar)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                ResetMessageStatus();
                // Cleared out messages, so reset the ItemSource to update the UI
                UnreadMessagesListView.ItemsSource = _messageItems;
            });
        }

        private void UnregisterDevice_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            try
            {
                // Send request to unregister the device
                string url = string.Format(App.ServiceBaseUrl + "RemoveDevice?userId={0}&deviceId={1}&deviceType={2}", _appSettings.UserId, _appSettings.DeviceId, "IOT");
                var request = WebRequest.Create(url);
                request.BeginGetResponse(ResponseRemoveDeviceCallback, request);
            }
            catch (Exception ex)
            {
                MessageStatus.Text += "; " + ex.Message;
            }
        }

        private async void ResponseRemoveDeviceCallback(IAsyncResult ar)
        {
            // TODO: Verify this is successful before returning the device to its "Factorry Settings"
            // Device should be unregistered, so we reset it back to its "Factory Settings"
            _appSettings.RestoreSettingsToDefaults();
            _appSettings.SaveSettings();
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                // Some nice clean up 
                ResetMessageStatus();

                // The device no longer has a need for the open pins, so we need to dispose them to free them up
                _redpin.Dispose();
                _bluepin.Dispose();

                // Kill the timer task so we stop polling the web service
                _timer.Stop();

                // Redirect to the initial setup page
                App.RootFrame.Navigate(typeof(LoginPage));
            });
        }

        private void ResetMessageStatus()
        {
            // Clear out messages
            _messageItems.Clear();

            // Clear out messages labels to their default text
            MessageCount.Text = "0 messages";
            MessageStatus.Text = "No messages";

            // On screen LED status should be "off" or changed to gray
            LED.Fill = _grayBrush;

            // Turn off LED
            _redpin.Write(GpioPinValue.Low);
            _bluepin.Write(GpioPinValue.Low);
        }
    }
}
