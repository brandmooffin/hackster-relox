﻿using Windows.Storage;

namespace ReloxIotApp
{
    public class AppSettings
    {
        #region Fields
        public int UserId;
        public string UserEmail = string.Empty;
        public int DeviceId;
        public string DeviceName = string.Empty;
        public string DeviceIdentifier = string.Empty;
        public bool IsActive;
        public int LastReadItemId;
        
        #endregion

        #region Initialzations
        public AppSettings()
        {
            LoadSettings();
        }

        #endregion

        public void RestoreSettingsToDefaults()
        {
            UserId = 0;
            UserEmail = string.Empty;
            DeviceId = 0;
            DeviceName = string.Empty;
            DeviceIdentifier = string.Empty;
            IsActive = false;
            LastReadItemId =0;
        }

        public void SaveSettings()
        {
            ApplicationData.Current.LocalSettings.Values["UserId"] = UserId;
            ApplicationData.Current.LocalSettings.Values["UserEmail"] = UserEmail;
            ApplicationData.Current.LocalSettings.Values["DeviceId"] = DeviceId;
            ApplicationData.Current.LocalSettings.Values["DeviceName"] = DeviceName;
            ApplicationData.Current.LocalSettings.Values["DeviceIdentifier"] = DeviceIdentifier;
            ApplicationData.Current.LocalSettings.Values["IsActive"] = IsActive;
            ApplicationData.Current.LocalSettings.Values["LastReadItemId"] = LastReadItemId;
        }

        public void LoadSettings()
        {
            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("UserId"))
            {
                UserId = (int)ApplicationData.Current.LocalSettings.Values["UserId"];
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("DeviceId"))
            {
                DeviceId = (int)ApplicationData.Current.LocalSettings.Values["DeviceId"];
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("DeviceIdentifier"))
            {
                DeviceIdentifier = ApplicationData.Current.LocalSettings.Values["DeviceIdentifier"].ToString();
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("DeviceName"))
            {
                DeviceName = ApplicationData.Current.LocalSettings.Values["DeviceName"].ToString();
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("UserEmail"))
            {
                UserEmail = ApplicationData.Current.LocalSettings.Values["UserEmail"].ToString();
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("IsActive"))
            {
                IsActive = (bool)ApplicationData.Current.LocalSettings.Values["IsActive"];
            }

            if (ApplicationData.Current.LocalSettings.Values.ContainsKey("LastReadItemId"))
            {
                LastReadItemId = (int)ApplicationData.Current.LocalSettings.Values["LastReadItemId"];
            }
        }
    }
}
